www.grandiaziendeinvictus.it
====================
Main website of Grandi Aziende Invictus srls company.

Developers
-----------
- Claudio Maradonna (claudio@unitoo.pw)

Designers
---------
- Stefano Amandonico (info@grafixprint.it)

License
-------
Licensed under AGPLv3 (see COPYING).

All images not listed below and logo have "all rights reserved".

`servizi.jpg`, `bg-nsb.jpg` licensed under CC BY-SA 4.0
